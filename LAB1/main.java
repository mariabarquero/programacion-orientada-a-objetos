//link de Repl.it https://repl.it/repls/MedicalJoyousRotation
//Maria Jose Barquero Perez-2019037947
class Main {
  public static void main(String[] args) {      //Clase main es la primera en ejecutarse

    Cuenta cliente1 = new Cuenta(1122,500000);  //Se crea un cliente con sus datos, con id y balance
    cliente1.settasaDeInteresAnual(0.045);
    cliente1.depositarDinero(150000);
    cliente1.retirarDinero(200000);
    System.out.println( "Cliente1"
                        +"\nBalance:"
                        +cliente1.getbalance()
                        +"\nTasa de Interes Mensual:"
                        +cliente1.obtenerTasaDeInteresMensual()
                        +"\nFecha de creacion: " 
                        +cliente1.getFechaDeCreacion());
    

    Cuenta cliente2 = new Cuenta(2244,800000);   //Se crea un cliente con sus datos,con id y balance
    cliente2.settasaDeInteresAnual(0.045);
    cliente2.depositarDinero(200000);
    cliente2.retirarDinero(100000);
    System.out.println( "Cliente2"
                        +"\nBalance:"
                        +cliente2.getbalance()
                        +"\nTasa de Interes Mensual:"
                        +cliente2.obtenerTasaDeInteresMensual()
                        +"\nFecha de creacion: " 
                        +cliente2.getFechaDeCreacion());


    ATM cajero = new ATM();  //Se crea un objeto cajero de tipo ATM
  }
  
}
