import java.util.Date;

public class Cuenta {   //Se crea la clase Cuenta 
  private int id = 0;
  private double balance = 0;
  private double tasaDeInteresAnual = 0;
  private Date FechaDeCreacion = new Date();

//Se creea un cosntructor sin argumentos
//Un constructor tiene mismo nombre que la clase,es publico,no retorna nada
public Cuenta( ) {   
  this.id = 0;
  this.balance =0;
  this.tasaDeInteresAnual = 0;
  this.FechaDeCreacion = new Date();
}

public Cuenta(int id,double balance){ //Se crea constructor con argumentos
  this.id = id;
  this.balance = balance;
  this.tasaDeInteresAnual = tasaDeInteresAnual;
}

//Se crean metodos get(para poder mostrar)y set(para poder modifcar)

public int getid(){
  return this.id;
}

public void setid(int id){
  this.id = id;
}

public double getbalance(){
  return this.balance;
}

public void setbalance(double balance){
  this.balance = balance; 
}

public double gettasaDeInteresAnual(){
  return this.tasaDeInteresAnual;
}

public void settasaDeInteresAnual(double tasaDeInteresAnual){
  this.tasaDeInteresAnual = tasaDeInteresAnual;
}

public Date getFechaDeCreacion(){
  return this.FechaDeCreacion;
}

//Metodo que retorna tasa de interes mensual
public double obtenerTasaDeInteresMensual(){
  double resultado = 0;
  resultado = tasaDeInteresAnual /12;
  return resultado;
  
}

//Metodo que retorna tasa de interes mensual con formula balance * tasaDeInteresAnual
public double calcularInteresMensual(){
  double resultado_interes = 0;
  resultado_interes = balance * tasaDeInteresAnual;
  return resultado_interes;
}

//Metodo que recibe una cantidad de dinero, y retira del balance 
public double retirarDinero(double retirar){
  balance = balance - retirar;
  return balance;
}

//Metodo que recibe una cantidad de dinero, y deposita en el balance 
public double depositarDinero(double depositar){
  balance = balance + depositar;
  return balance;
}

}
