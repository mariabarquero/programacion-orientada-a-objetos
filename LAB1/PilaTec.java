import java.util.ArrayList;

public class PilaTec extends ArrayList{      //Se hace un extends a Arraylist convirtiendo PilaTec en un Arraylist


  public PilaTec(){                         //Se crea un constructor donde se llama a super para poder invocar a metodos de la super clase 
    super();
  }

  public boolean isEmpty(){
    return super.isEmpty();
  }

  public int getSize(){
    return super.size();
  }

  public Object peek(){
    return super.get(getSize()-1);
  }

  public Object pop(){
    Object o = super.get(getSize()-1); super.remove(getSize()-1);
    return o;
  }

  public void push(Object o){
    super.add(o);
  }

  @Override
  public String toString(){
    return "pila: "+super.toString();

  }
}