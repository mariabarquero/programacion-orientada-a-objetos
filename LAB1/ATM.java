import java.util.Scanner;

public class ATM{    //Se crea una clase ATM
  public ATM(){      //Se crea un constructor

    Cuenta[] arreglosCliente = new Cuenta[10];    //Se crea un arreglo de 10 espacios
    arreglosCliente[0] = new Cuenta(0,100000);    //Se le asigna a cada espacio una cuenta
    arreglosCliente[1] = new Cuenta(1,100000);
    arreglosCliente[2] = new Cuenta(2,100000);
    arreglosCliente[3] = new Cuenta(3,100000);
    arreglosCliente[4] = new Cuenta(4,100000);
    arreglosCliente[5] = new Cuenta(5,100000);
    arreglosCliente[6] = new Cuenta(6,100000);
    arreglosCliente[7] = new Cuenta(7,100000);
    arreglosCliente[8] = new Cuenta(8,100000);
    arreglosCliente[9] = new Cuenta(9,100000);

    
    Scanner entrada = new Scanner(System.in);   //Se crea entrada para recibir opcion de usuario
    int opcion;
    int num;
    boolean salir = false;
    boolean ciclo = true;
    double retirar;
    double depositar;
    

    while(ciclo == true){    //Se crea un ciclo que no permita salirse del programa
       System.out.println("Ingrese su Id:"); //Se solicita al usuario el id
       num = entrada.nextInt();              //Se guarda el numero que digito

       if (num > -1 && num < 10) {          //Verifica que el numero este entre 0 y 9
         salir = false;
         while(!salir){                     //Se muestra un menu
            System.out.println("Menu Principal");
            System.out.println("1. Ver el balance actual");
            System.out.println("2. Retirar Dinero");
            System.out.println("3. Depositar Dinero");
            System.out.println("4. Salir");
            
            System.out.println("Escribe una de las opciones"); //Se solicita elegir una opcion
            opcion = entrada.nextInt();                        //Se guarda la opcion
            switch(opcion){ 
              case 1:
               System.out.println("El balance es de:"+arreglosCliente[num].getbalance());
               break;
              case 2:
                System.out.println("Ingrese una cantidad para retirar");
                retirar = entrada.nextDouble();
                arreglosCliente[num].retirarDinero(retirar);
                break;
              case 3:
                System.out.println("Ingrese una cantidad para depositar");
                depositar = entrada.nextDouble();
                arreglosCliente[num].depositarDinero(depositar);
                break;
              case 4:
                System.out.println("El balance es de:"+arreglosCliente[num].getbalance());
                System.out.println("Salio del programa");
                salir=true;
                break;
              default:
                System.out.println("Solo números entre 1 y 4");
            }
           }
         }else{
           System.out.println("Id invalido");
           }
      }
    
  }
}
