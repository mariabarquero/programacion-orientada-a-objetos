/**********************************************************************
 Instituto Tecnológico de Costa Rica
 Programacion Orientada a Objetos
 II Semestre 2019
 Profesora: Samanta Ramijan Carmiol
 Laboratorio 10

 Estudiantes:
 María José Barquero Pérez
 2019037947

 **********************************************************************/
package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("VentanaSaludo.fxml"));
        primaryStage.setTitle("Primer Interfaz");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
