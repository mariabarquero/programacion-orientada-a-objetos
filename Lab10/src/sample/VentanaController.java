package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class VentanaController {

    public TextField txtNombre;     //Nuevo atributo
    public Label lblSaludo;

    public void botonclick(ActionEvent actionEvent) {
        String nombre = txtNombre.getText();
        lblSaludo.setText("Bienvenido " + nombre);
    }
}
