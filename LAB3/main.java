//Maria Jose Barquero Perez-2019037947
//https://repl.it/@mariajose8/UniqueUncomfortableVisitor

import java.util.Scanner;

class Main {
  public static void main(String[] args) {
    PilaTec pila = new PilaTec();
    Scanner  input = new Scanner(System.in);
    int indice = 0;
    int num = 0;
    while (indice < 5){
      System.out.print("Ingrese un numero entero: \n");
      num = input.nextInt();
      pila.push(num);
      
      indice++;
    }

    System.out.print("\nLos numero ingresados del ultimo al primero son:\n");
    for(int i=0;i<5;i++){
    System.out.print(pila.pop());
    }
  }
}
