IC-2101 Programación Orientada a Objetos
Prof: MSc. Samanta Ramijan Carmiol
Laboratorio 11
Comprensión de Código y Archivos
	Estudiantes:
                   Maria Jose Barquero Perez
                   Te Chen 
                   Randall Zumbado Huertas 

En el laboratorio 7 realizamos un ejercicio de modelado para un pequeño Supermercado, en
este laboratorio se espera a partir del código disponible.
a. Importe en su proyecto las clases:
● Carrito.java
● Cliente.java
● Factura.java
● Membresia.java
● Producto.java
● Promocion.java
b. Cree un archivo ​ productos.xls ​ que contenga la información de al menos 10
productos
c. Cree una clase ​ AdministradorArchivo.java que contenga un método que
permita cargar el archivo de productos en la memoria del programa. Para esto
investigue acerca del manejo de archivos en Java y recuerde manejar las posibles
excepciones.
d. Cree su propia clase ​ Main.java que cree una instancia de la clase supermercado,
utilice la clase AdministradorArchivo.java para popular los productos y
solicite al usuario el resto de la información restante de promociones y membresías
para configurar el supermercado.Aspectos Administrativos

1.Límite para la entrega de la asignación: Jueves 17 de octubre a las 3pm.
2. Plataforma de revisión: repositorio de código.
