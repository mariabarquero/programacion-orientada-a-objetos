/*Estudiantes:  Raychel Arguedas Bolívar 
                María José Barquero Pérez
                Te Chen Huang
*/
import java.util.ArrayList;
import java.util.Scanner;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main{

	public static void main(String[] args) {
	    Scanner  input = new Scanner(System.in);
	    String nombre;
	    int ID;
	    int opcion;
      String Labor;
      String Horario;
	    ArrayList<Policia> listapolicias = new ArrayList<Policia>();
	    ArrayList<Actividad> Registro = new ArrayList<Actividad>();


  System.out.println("--------------------------------------\n");
  System.out.println("Menú principal\n Escoja una opcion \n");
  System.out.println("\t1. Agregar un policía");
  System.out.println("\t2. Imprimir lista de polícia"); 
  System.out.println("\t3. Salir\n");
  System.out.println("--------------------------------------");

  while(true){
    System.out.println("\nIngrese su selección"); //elegir una opcióón
    opcion = input.nextInt(); //guardar opción
    
    switch(opcion){
      case 1:
	      System.out.println("Ingresar Nombre de Policia");
	        nombre = input.next();

	      System.out.println("Ingrese numero de Identificacion");
	        ID = input.nextInt();	     

	      System.out.println("Labor:");
          Labor = input.next();

        System.out.println("Regular/Particular");
          Horario = input.next();

        System.out.println("Introduzca la fecha que va a trabajar con formato dd/mm/yyyy");

        Scanner sc = new Scanner(System.in);
        String fecha = sc.nextLine();

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String date = fecha;

        System.out.println("\nNombre:" +nombre +
                            "\tID:" +ID + 
                            "\tLabor:"  +Labor +
                            "\tHorario:" + Horario + 
                            "\tFecha:"+date);
                            
         // Agrega la informacion del policia y de las actividades a sus respectivas listas                   
        listapolicias.add(new Policia(ID,nombre));
        Registro.add(new Actividad(Labor,Horario));
        break;        
        

      case 2:
        for(int i = 0; i< listapolicias.size(); i++){
            System.out.println("---------------------------------------------");

            System.out.println("\tEl administrador ha actualizado con exito los datos\n");

            System.out.println("La informacion del policia en la posicion " + i + " es la siguiente: \n");

            System.out.println("\tNombre de Policia: "+
                                listapolicias.get(i).getNombre());

            System.out.println("\tIdentificacion: "+
                                listapolicias.get(i).getID());

            System.out.println("\tLabor: "+
                                Registro.get(i).getLabor());

            System.out.println("\tHorario: "+
                                Registro.get(i).getHorario());

            System.out.println("---------------------------------------------");
        }
        break;
        
      case 3:
        System.exit(0);
            break;
//opcion invalida
      default:
          System.out.println("Las opciones son entre 1 y 3");
          break;
      }
    }
  }
}
